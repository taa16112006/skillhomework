
public class Printer {
    private String queue = "";
    private int countPages = 0;  // общее количество страниц, добавленных в принтер, но ещё не распечатанных
    private int countDocuments = 0; // общее количество документов, которые добавлены в принтер, но ещё не распечатаны,
    private int totalCountFinalDocuments = 0; //общее количество распечатанных документов за всё время существования объекта класса
    private int totalCountFinalPages = 0; ///общее количество распечатанных страниц за всё время существования объекта класса

    public static void main(String[] args) {
        Printer printer = new Printer();
        printer.append("Doc");
        printer.clear();
        printer.append("Doc");
        printer.print();
        printer.append("Doc");
        printer.getPagesCount();
        printer.getDocumentsCount();
        printer.getFinalCountDocsPages();
    }
    public void append(String text)             { append(text, 1); }
    public void append(String text, String name){ append(text, name,1); }
    public void append(String text, String name, int pages){
        queue = queue + "Текст:" + text + " " + "Название:" + name + " " +  "Количество страниц:"+ pages +  "\n";
        countPages = countPages + pages;
        countDocuments ++;
    }

    public void append(String text, int pages){
        queue = queue + "Текст:" + text + " " +  "Количество страниц:" + pages +"\n";
        countPages = countPages + pages;
        countDocuments ++;
    }

    public void getPagesCount() {
        System.out.println("Общее количество страниц:" + countPages);
    }
    public void getDocumentsCount(){
        System.out.println("Общее количество документов:" + countDocuments);
    }
    public void getFinalCountDocsPages(){
        System.out.println("Общее количество распечатанных документов за все время:" + totalCountFinalDocuments);
        System.out.println("Общее количество распечатанных страниц за все время:" + totalCountFinalPages);
    }

    public void print(){
        System.out.println(queue);
        totalCountFinalDocuments=totalCountFinalDocuments+countDocuments;
        totalCountFinalPages=totalCountFinalPages+countPages;
        clear();
    }
    public void clear(){
        queue = "";
        countPages = 0;
        countDocuments = 0;
    }
}
